﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityController : MonoBehaviour {
	private bool gravity_enabled = true;
	private bool manual_gravity = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(OVRInput.GetDown(OVRInput.RawButton.RIndexTrigger)){
			if (gravity_enabled) {
				Physics.gravity = new Vector3 (0f, 10.0f, 0f);
				gravity_enabled = false;
			} else {
				Physics.gravity = new Vector3 (0f, -10.0f, 0f);
				gravity_enabled = true;
			}
		}

		if (OVRInput.GetDown (OVRInput.RawButton.LIndexTrigger)) {
			if (manual_gravity == true) {
				manual_gravity = false;
			} else {
				manual_gravity = true;
			}
		}

		if (manual_gravity == true) {
			Physics.gravity = OVRInput.GetLocalControllerAcceleration (OVRInput.Controller.LTouch);
		} else {
			if (gravity_enabled) {
				Physics.gravity = new Vector3 (0f, 10.0f, 0f);
			} else {
				Physics.gravity = new Vector3 (0f, -10.0f, 0f);
			}
		}
	}
}
