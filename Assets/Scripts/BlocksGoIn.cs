﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlocksGoIn : MonoBehaviour {
	[SerializeField]
	private Vector3 direction;

	[SerializeField]
	private float max_distance = 0.1f;

	[SerializeField]
	private float min_distance = 0.0001f;

	[SerializeField]
	private float delay_time = 0f;

	private bool going_in = true;
	private bool movement_enabled = false;

	[SerializeField]
	private float max_speed = 0.1f;
	// Use this for initialization
	void Start () {
		StartCoroutine(enableMovement ());
	}

	IEnumerator enableMovement() {
		yield return new WaitForSeconds (delay_time);
		movement_enabled = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (movement_enabled == true) {
			if (going_in == true) {
				transform.Translate (direction * Time.deltaTime * max_speed);
				if ((Vector3.Distance (Vector3.zero, transform.localPosition) > max_distance)) {
					going_in = false;
				}
			} else {
				transform.Translate (-direction * Time.deltaTime * max_speed);
				if ((Vector3.Distance (Vector3.zero, transform.localPosition) <= min_distance)) {
					going_in = true;
				}
			}
		}
	}
}
