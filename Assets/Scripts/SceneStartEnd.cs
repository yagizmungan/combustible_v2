﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneStartEnd : MonoBehaviour {

	[SerializeField]
	private int end_type = 0;

	[SerializeField]
	private GameObject roof;
	[SerializeField]
	private float disable_roof_time = 540;

	// Use this for initialization
	void Start () {
		Time.timeScale = 0;
		switch (end_type) {
		case 0:
			StartCoroutine (disableRoofCollider ());
			break;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space)) {
			Time.timeScale = 1;
		}
	}

	IEnumerator disableRoofCollider() {
		Debug.Log ("end 1");
		yield return new WaitForSeconds (disable_roof_time);

		roof.GetComponent<MeshCollider>().enabled = false;
		Physics.gravity = new Vector3 (0f, -5.0f, 0f);
	}
}
