﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NewtonVR;

public class RagdollTrigger : MonoBehaviour {


	private float impact_end_time = 0;
	private Rigidbody impact_target = null;
	private Vector3 impact;

	private RagdollHelper ragdoll_helper;

	[SerializeField] 
	private string target_tag = "";
	[SerializeField]
	private GameObject free_grab_body_part;
	[SerializeField]
	private bool attach_NVR = false;

	public NewtonVR.NVRInteractableItem NVR_interactable_script;

	// Use this for initialization
	void Start () {
		ragdoll_helper = gameObject.GetComponent<RagdollHelper> ();
		Rigidbody[] rigidBodies=GetComponentsInChildren<Rigidbody>();

		foreach (Rigidbody body in rigidBodies)
		{
			RagdollPart ragdoll_part = body.gameObject.AddComponent<RagdollPart>();
			ragdoll_part.target_tag = target_tag;
			ragdoll_part.ragdoll_trigger_script = this;

			if (attach_NVR == true) {
				body.gameObject.AddComponent<NewtonVR.NVRInteractableItem> ();
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		//Check if we need to apply an impact
		if (Time.time < impact_end_time)
		{
			impact_target.AddForce(impact,ForceMode.VelocityChange);
		}

		if(Input.GetKeyDown(KeyCode.Z) || OVRInput.GetDown (OVRInput.RawButton.B) || OVRInput.GetDown (OVRInput.RawButton.Y)) {
			setToMechanim ();
		}
	}
		
	public void setToRagdoll (GameObject body_part, Vector3 impact_vector){		
		ragdoll_helper.ragdolled = true;
		impact_target = body_part.GetComponent<Rigidbody>();
		impact = impact_vector; 
		impact_end_time = Time.time + 0.25f;
		gameObject.GetComponent<AudioSource> ().Stop ();
	}

	public void setToFreeRagdoll (){	
		// falls to the ground...
//		setToRagdoll (free_grab_body_part,Vector3.zero);
//		ragdoll_helper.ragdolled = true;
	}

	public void setToMechanim () {
		ragdoll_helper.ragdolled = false;
	}
}
