﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateWalls : MonoBehaviour {

	[SerializeField]
	private GameObject referenceObject;
	[SerializeField]
	private int number_of_rows = 5;
	[SerializeField]
	private int number_of_columns = 5;
	[SerializeField]
	private float horizontal_space = 1f;
	[SerializeField]
	private float vertical_space = 1f;
	[SerializeField]
	private Vector3 gravity_vector = new Vector3(0f, 0f, 0f);

	private GameObject parent;
	private GameObject previous = null;

	private GameObject[,] wall_objects;
//	private GameObject[,] wall_objects = new GameObject[number_of_rows, number_of_columns];
//	private GameObject[,] wall_objects = new GameObject[10,10];

	// Use this for initialization
	void Start () {
		wall_objects =  new GameObject[number_of_rows, number_of_columns];
		Physics.gravity = gravity_vector;
		parent = gameObject;
		float starting_x = -number_of_columns * horizontal_space / 2; 
		float starting_y = number_of_rows * vertical_space / 2;
		float starting_z = parent.transform.position.z;
		for (int i = 0; i < number_of_columns; i++){
			for (int j = 0; j < number_of_rows; j++){
				Vector3 instantiate_position = new Vector3 (starting_x + i * horizontal_space, starting_y - j * vertical_space, starting_z);
				GameObject instantiation = GameObject.Instantiate (referenceObject, instantiate_position, Quaternion.identity, parent.transform);

				if (j == 0 || j == number_of_columns-1) {
					instantiation.GetComponent<Rigidbody> ().isKinematic = true;
					instantiation.GetComponent<Rigidbody> ().useGravity = false;
				} 
				if( j != 0) {
					instantiation.GetComponents<SpringJoint>()[0].connectedBody = previous.GetComponent<Rigidbody>();
					if (i > 0) {						
						instantiation.GetComponents<SpringJoint> () [1].connectedBody = wall_objects [i - 1, j].GetComponent<Rigidbody> ();
					}
				}
				wall_objects [i,j] = instantiation;
				previous = instantiation;
			}
		}

		for (int j = 0; j < number_of_rows; j++) {
			for (int i = 0; i < number_of_columns; i++) {
				int left = i - 1;
				if(left > 0){
					GameObject temp = new GameObject ();
					temp.name = "left spring joint";
					temp.AddComponent<SpringJoint> ();
					temp.transform.SetParent (wall_objects [i, j].transform);
					temp.GetComponent<SpringJoint>().connectedBody = wall_objects[left, j].GetComponent<Rigidbody>();
				}

				int right = i + 1;
				if (right < number_of_rows) {
					GameObject temp2 = new GameObject ();
					temp2.name = "right spring joint";
					temp2.AddComponent<SpringJoint> ();
					temp2.transform.SetParent (wall_objects [i, j].transform);
					temp2.GetComponent<SpringJoint>().connectedBody = wall_objects[right, j].GetComponent<Rigidbody>();
				}
			}
		}
			
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
