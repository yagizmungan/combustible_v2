using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollPart : MonoBehaviour {

	public string target_tag = "projectile";
	[SerializeField]
	private float impact_scalar = 0.2f;

	public RagdollTrigger ragdoll_trigger_script;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider other) {
		// tag filtering
		if (other.gameObject.tag == target_tag || target_tag == "") {
			if (other.GetComponent<Rigidbody> () == null) {
				other.gameObject.AddComponent<Rigidbody> ();
			}
			Vector3 impact_vector = other.GetComponent<Rigidbody>().velocity  * impact_scalar;

			ragdoll_trigger_script.setToRagdoll (gameObject, impact_vector);
		}
	}

	void OnCollisionEnter(Collision collision) {
		// tag filtering
		if (collision.gameObject.tag == target_tag || target_tag == "") {
			if (collision.gameObject.GetComponent<Rigidbody> () == null) {
				collision.gameObject.gameObject.AddComponent<Rigidbody> ();
			}
			Vector3 impact_vector = collision.gameObject.GetComponent<Rigidbody>().velocity  * impact_scalar;

			ragdoll_trigger_script.setToRagdoll (gameObject, impact_vector);
		}
	}
}
