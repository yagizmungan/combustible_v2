﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateOnButtonPress : MonoBehaviour {

	public GameObject[] reference_objects;

	[SerializeField]
	private bool left_hand_enabled = false;
	[SerializeField]
	private bool right_hand_enabled = false;
	[SerializeField]
	private bool is_trigger = false;
	[SerializeField]
	private bool on_ground = false;
	[SerializeField]
	private bool face_user = false;

	[SerializeField]
	private bool randomize_animation = false;
	[SerializeField]
	private RuntimeAnimatorController[] animation_controllers;

	[SerializeField]
	private bool scale_spawned = false;
	[SerializeField]
	private float min_regular_scale = 0.9f;
	[SerializeField]
	private float max_regular_scale = 1.1f;
	[SerializeField]
	private float unique_scale_chance = 0.1f;
	[SerializeField]
	private float unique_scale = 2.0f;


	private int number_of_suits = 0;
	private int number_of_large_suits = 0;



	private Camera main_camera;

	// Use this for initialization
	void Start () {
		main_camera = Camera.main;	
	}
	
	// Update is called once per frame
	void Update () {
		if (OVRInput.GetDown (OVRInput.RawButton.A) && right_hand_enabled) {
			Vector3 temp_position = OVRInput.GetLocalControllerPosition (OVRInput.Controller.RTouch);
			spawnORandobject(temp_position);
		}

		if (OVRInput.GetDown (OVRInput.RawButton.X) && left_hand_enabled) {
			Vector3 temp_position = OVRInput.GetLocalControllerPosition (OVRInput.Controller.LTouch);
			spawnORandobject(temp_position);
		}
	}

	void spawnORandobject(Vector3 hand_position) {
		int index = Random.Range (0, reference_objects.Length);

		GameObject spawned_object = Instantiate (reference_objects [index], hand_position, Quaternion.identity);

		if (scale_spawned == true) {
			float target_scale = Random.Range (min_regular_scale, max_regular_scale);
			spawned_object.transform.localScale = new Vector3 (target_scale, target_scale, target_scale);

			number_of_suits += 1;

			if (Random.value <= unique_scale_chance && number_of_suits > 15 && number_of_large_suits <= 2) {
				number_of_large_suits += 1;
				spawned_object.transform.localScale = new Vector3 (unique_scale, unique_scale, unique_scale);
			}
		}

		if (on_ground == true) {
			Vector3 spawn_position = spawned_object.transform.position;
			spawn_position.y = 0;
			spawned_object.transform.position = spawn_position;
		}

		if (face_user == true) {
			Vector3 temp_location = Camera.main.transform.position;//main_camera.transform.position;
			temp_location.y = 0;
			spawned_object.transform.LookAt (temp_location);
		}

		if (randomize_animation == true) {
			int animator_index = Random.Range (0, animation_controllers.Length);
			spawned_object.GetComponent<Animator> ().runtimeAnimatorController = animation_controllers [animator_index] as RuntimeAnimatorController;
		}
		spawned_object.GetComponent<Collider> ().isTrigger = is_trigger;
	}
}
