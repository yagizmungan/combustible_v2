﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicalHand : MonoBehaviour {


	[SerializeField]
	private GameObject right_hand;
	[SerializeField]
	private GameObject left_hand;

	private NewtonVR.NVRPhysicalController physical_right_hand;
	private NewtonVR.NVRPhysicalController physical_left_hand;

	private bool right_physical = false;
	private bool left_physical = false;
	private bool init = false;

	// Use this for initialization
	void Start () {
		StartCoroutine (findPhysicalHandScript ());
	}

	IEnumerator findPhysicalHandScript() {
		yield return new WaitForSeconds (5);
		physical_right_hand = right_hand.GetComponent<NewtonVR.NVRPhysicalController> ();
		physical_left_hand = left_hand.GetComponent<NewtonVR.NVRPhysicalController> ();
		init = true;
	}
	// Update is called once per frame
	void Update () {
		if (init == false) {
			return;
		}

		if (OVRInput.Get(OVRInput.RawAxis2D.RThumbstick).x > 0.9f) {
			physical_right_hand.On();
			right_physical = true;
		} else {
			if (right_physical == true) {
				physical_right_hand.Off();
				right_physical = false;
			}
		}

		if (OVRInput.Get(OVRInput.RawAxis2D.LThumbstick).x < -0.9f) {
			physical_left_hand.On();
			left_physical = true;
		} else {
			if (left_physical == true) {
				physical_left_hand.Off ();
				left_physical = false;
			}
		}
	
	}
}
