﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class ScreenSpawner : MonoBehaviour {
	[SerializeField]
	private float max_x = 5f;
	[SerializeField]
	private float max_z = 5f;
	[SerializeField]
	private float min_x = -5f;
	[SerializeField]
	private float min_z = -5f;

	[SerializeField]
	private GameObject screen;

	[SerializeField]
	private VideoClip[] videos;
	[SerializeField]
	private Material[] video_materials;

	[SerializeField]
	private bool random_scale = true;
	[SerializeField]
	private float min_scale = 1;
	[SerializeField]
	private float max_scale = 3;

	[SerializeField]
	private float screen_rise_speed = 5;

	[SerializeField]
	private bool look_at_user = true;

	[SerializeField]
	private float startup_delay = 0f;


	private bool screen_rising = false;
	private bool screen_sinking = false;

	// Use this for initialization
	void Start () {
		StartCoroutine (startup ());
	}

	IEnumerator startup() {
		yield return new WaitForSeconds(startup_delay);
		screen.GetComponent<MeshRenderer> ().enabled = true;
		setRandomVideo ();
		showVideoPlane ();
	}
	
	// Update is called once per frame
	void Update () {
		if (screen_rising) {
			screen.transform.Translate(Vector3.up * Time.deltaTime * screen_rise_speed);
			if (screen.transform.position.y >= screen.transform.localScale.x / 2) {
				screen_rising = false;
			}
		}
		if (screen_sinking) {
			screen.transform.Translate(Vector3.down * Time.deltaTime * screen_rise_speed);
			if (screen.transform.position.y <= -screen.transform.localScale.x / 2) {
				screen_sinking = false;
				setRandomVideo ();
				showVideoPlane ();
			}
		}
	}


	void showVideoPlane() {
		// set location
		Vector3 new_location = screen.transform.position;
		new_location.x = Random.Range (min_x, max_x);
		new_location.z = Random.Range (min_z, max_z);
		new_location.y = 0;	

		if (random_scale) {
			float scale_factor = Random.Range (min_scale, max_scale);
			screen.transform.localScale = new Vector3 (scale_factor, scale_factor, scale_factor);
			new_location.y = -scale_factor / 2;
		}

		screen.transform.position = new_location;
		if (look_at_user == true) {
			lookAtUser (screen);
		}

		// start
		screen.GetComponent<VideoPlayer> ().Play();

		// raise
		screen_rising = true;
	}

	void hideVideoPlane() {
		screen_sinking = true;
	}

	void EndReached(UnityEngine.Video.VideoPlayer vp) {
		hideVideoPlane();
	}

	void lookAtUser(GameObject looking_object) {
		Vector3 temp_location = Camera.main.transform.position;//main_camera.transform.position;
		temp_location.y = looking_object.transform.position.y;
		looking_object.transform.LookAt (temp_location);
	}

	void setRandomVideo() {
		int index = Random.Range (0, videos.Length);
		screen.GetComponent<Renderer> ().material = video_materials [index];
		screen.GetComponent<VideoPlayer> ().clip = videos [index];
		screen.GetComponent<VideoPlayer> ().loopPointReached += EndReached;
	}
}
