﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueSuitAudio : MonoBehaviour {

	private AudioSource audio_source;
	[SerializeField]
	private AudioClip[] audio_clips;

	// Use this for initialization
	void Start () {
		audio_source = gameObject.GetComponent<AudioSource> ();
		audio_source.clip = audio_clips[Random.Range(0, audio_clips.Length)];
		audio_source.Play();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void stopAudio() {
		audio_source.Stop ();
	}
}
