﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraHijack : MonoBehaviour {

	public GameObject vr_camera;
	public GameObject regular_camera;

	public Transform look_at_target;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		vr_camera.SetActive (false);
		regular_camera.SetActive (true);
		regular_camera.transform.LookAt (look_at_target);
	}

	void LateUpdate () {
		vr_camera.SetActive (false);
		regular_camera.SetActive (true);
		regular_camera.transform.LookAt (look_at_target);
	}
}
