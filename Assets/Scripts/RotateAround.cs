﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAround : MonoBehaviour {
	[SerializeField]
	Vector3 rotation_vector = new Vector3(0.001f, 0, 0);

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate (rotation_vector * Time.deltaTime);
	}
}
